provider "aws" {
  region = "us-east-2"
}

module "ops-app" {
  source        = "git::https://gitlab.com/pavanannisetty/ec2_module.git"
  ami           = "ami-05d72852800cbf29e"
  instance_type = "t2.micro"
  key_pair      = "testaccount"
  subnet        = "subnet-0a788f6b371b27bd4"
  environment   = "ops"
  client        = "pavan"
  server        = "app"
  vpc_id        = "vpc-043d01580e0d271df"
}

output "PrivateIP" {
  description = "Private IP of EC2 instance"
  value       = module.ops-app.private_ip
}

output "Name" {
  description = "Private IP of EC2 instance"
  value       = module.ops-app.tags
}
